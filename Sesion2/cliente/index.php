<html>
	<head>
		<title>Workshop WWC Barcelona - Websockets</title>
		<link rel="stylesheet" href="css/bootstrap.min.css">
		<script src="js/jquery-3.3.1.min.js"></script>
		<script src="js/bootstrap.min.js"></script>
		<script src="js/chatfunctions.js"></script>
	</head>
	<body>
		<div class="container-fluid">
		  	<!-- Content here -->
			<nav class="navbar navbar-expand-lg navbar-light bg-light">
			  	<a class="navbar-brand" href="#">Websockets</a>
			  		<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
			    	<span class="navbar-toggler-icon"></span>
			 	</button>

			  	<div class="collapse navbar-collapse" id="navbarSupportedContent">
			    	<ul class="navbar-nav mr-auto">
				      	<li class="nav-item itemsLogged" style="display: none;">
				        	<a class="nav-link" id="viewProfile" href="javascript:void(0);">My Profile <span class="sr-only">(current)</span></a>
				      	</li>
				      	<li class="nav-item itemsLogged" style="display: none;">
				        	<a class="nav-link" id="createChannel" href="javascript:void(0);">New channel</span></a>
				      	</li>

				      	<li class="nav-item itemsOff">
				        	<a class="nav-link" id="viewProfile" href="https://bitbucket.org/monica_team/wwcb-websockets-1/src/master/" target="_blank">Source code</a>
				      	</li>
				    </ul>
				    <span class="navbar-text itemsLogged" style="display: none;">
				    	Logged users: <span id="countUsers"></span>
				    </span>
			  	</div>
			</nav>

			<div class="alert alert-primary alertsConnection" id="conectionPending" role="alert" style="display:none;">
			  Connecting to server...
			</div>

			<div class="alert alert-danger alertsConnection" id="conectionError" role="alert" style="display:none;">
			 	Error while connecting to server
			</div>

			<div class="alert alert-success alertsConnection" id="conectionOK" role="alert" style="display:none;">
			 	Connected to the server
			</div>


			<div class="modal hide" tabindex="-1" id="profileDetails" role="dialog" data-backdrop="static" data-keyboard="false">
			  	<div class="modal-dialog" role="document">
			   		<div class="modal-content">
			      		<div class="modal-header">
			       			<h5 class="modal-title">My Profile</h5>
			        		<button type="button" class="close" data-dismiss="modal" aria-label="Close">
			          			<span aria-hidden="true">&times;</span>
			        		</button>
			      		</div>
			      		<div class="modal-body">
			        		<p>Connection details:</p>
			        		<div class="form-group">
							    <label for="changeNickname">Name</label>
							    <input type="text" class="form-control" id="changeNickname" aria-describedby="emailHelp">
							</div>
							<div class="form-group">
							    <label for="changeDescription">Public description</label>
							    <input type="text" class="form-control" id="changeDescription">
							</div>
			      		</div>
			      		<div class="modal-footer">
					        <button type="button" id="modifyProfile" class="btn btn-primary">Save changes</button>
					        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
			      		</div>
			    	</div>
			  	</div>
			</div>

			<div class="modal hide" tabindex="-1" id="profileWelcome" role="dialog" data-backdrop="static" data-keyboard="false">
			  	<div class="modal-dialog" role="document">
			   		<div class="modal-content">
			      		<div class="modal-header">
			       			<h5 class="modal-title">Welcome!</h5>
			      		</div>
			      		<div class="modal-body">
			        		<p>Please enter your desired nickname!</p>
			        		<input type="text" class="form-control" id="newName" placeholder="New nickname" />
			      		</div>
			      		<div class="modal-footer">
					        <button type="button" id="firstWelcome" class="btn btn-primary">Save changes</button>
			      		</div>
			    	</div>
			  	</div>
			</div>

			<div class="modal hide" tabindex="-1" id="dialogCreateChannel" role="dialog" data-backdrop="static" data-keyboard="false">
			  	<div class="modal-dialog" role="document">
			   		<div class="modal-content">
			      		<div class="modal-header">
			       			<h5 class="modal-title">Creating a channel</h5>
			      		</div>
			      		<div class="modal-body">
			        		<p>Enter the desired channel name</p>
			        		<input type="text" class="form-control" id="newChannelName" placeholder="New channel name" />
			      		</div>
			      		<div class="modal-footer">
					        <button type="button" id="newChannel" class="btn btn-primary">Save changes</button>
					        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
			      		</div>
			    	</div>
			  	</div>
			</div>


			<div class="card" id="connectionDetails">
			  	<div class="card-header">
			    	Connect to websocket server
			  	</div>
			 	<div class="card-body">
				    <h5 class="card-title">Write the IP or hostname, of the websocket server</h5>
				    <p class="card-text">If you're gonna connect local, write 127.0.0.1, if you know the IP write it, else write the hostname</p>
				    <p class="card-text"><input type="text" class="form-control" id="ipDestination" placeholder="127.0.0.1"></p>
				    <a href="javascript:void(0);" id="changeConectionDestination" class="btn btn-primary">Connect!</a>
			  	</div>
			</div>


		  	<div class="row" id="chatContent" style="display:none;">
			    <div class="col-2">
		      		<div class="card">
					  	<div class="card-header">
					   		<h3>Channels</h3>
					  	</div>
					  	<ul class="list-group list-group-flush" id="channelList">

					 	</ul>
					</div>
			    </div>
			    <div class="col-10" id="chatContainer" style="display:none;">
			      	<div class="card">
					  	<div class="card-header" id="channelName">
					    	
					  	</div>
					  	<div class="card-body" id="channelText">

					  	</div>
					  	<div class="card-footer text-muted">
					  		<div class="row">
						  		<div class="col-10">
						    		<input type="text" class="form-control" id="textChat">
						    	</div>
						    	<div class="col-2">
						    		<button type="button" id="sendTextChat" class="btn btn-primary">Send</button>
						    	</div>
						    </div>
					  	</div>
					</div>
			    </div>
		  	</div>

		</div>
	</body>
</html>