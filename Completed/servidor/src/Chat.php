<?php

namespace WorkshopWebsocketsWWCB;
use Ratchet\MessageComponentInterface;
use Ratchet\ConnectionInterface;

class Chat implements MessageComponentInterface {
	private $_connections = array();
	private $_chatRooms = array();
	private $_onlineData = array();

	private $_joinedUsersRoom = array();

	/*
	--- MESSAGE ID's responses from server----
	1. Open new user connected
	2. Error control dialog
	3. Add text to chatroom
	4. Create chatroom on clientside
	5. Dialog of profile data
	6. Update current room list name
	7. Users list
	8. Update toolbar
	*/


	/*
	Function: onOpen
	It triggers after a new connections enters to the websocket server
	*/
    public function onOpen(ConnectionInterface $conn) {
    	$conn->Name = "";
    	$conn->publicDescription = "";
    	echo "[New connection] ConectionID: ".$conn->resourceId."\n";
    	$this->_connections[$conn->resourceId] = $conn;
    	$conn->send(json_encode(array(
    		"message_id"=>1,
    		"registered"=>0,
    		"error"=>0
    	)));
    	//---UPDATE global count---
    	$this->updateGLobalUserCount();
    }

    private function updateGLobalUserCount() {
    	$totalOnline = count($this->_onlineData);
    	foreach ($this->_connections as $conn) {
    		$conn->send(json_encode(array(
	    		"message_id"=>8,
	    		"totalUsers"=>$totalOnline
	    	)));
    	}
    	
    }

    public function onMessage(ConnectionInterface $from, $msg) {
    	$messageData = json_decode($msg);
    	$conectionID = $from->resourceId;

    	switch ($messageData->tipo) {
    		case 'welcome_data':
    			if(isset($messageData->nickname)) {
    				if(strlen($messageData->nickname) > 0) {
	    				$this->_connections[$conectionID]->Name = $messageData->nickname;
	    				$from->send(json_encode(array(
				    		"message_id"=>1,
				    		"registered"=>1,
				    		"error"=>0
				    	)));
				    	$this->_joinedUsersRoom[$conectionID] = array();
				    	$this->_onlineData[$conectionID] = $this->_connections[$conectionID]->Name;
				    	$this->showCurrentRoomsList($conectionID);
				    	$this->updateGLobalUserCount();
				    }
				    else {
				    	//No nickname received
				    	$from->send(json_encode(array(
				    		"message_id"=>1,
				    		"registered"=>0,
				    		"error"=>1,
				    		"error_txt"=>"Nickname lenght is 0"
				    	)));
				    }
    			}
    			break;
    		
    		case 'createRoom':
    			if(isset($messageData->roomName) && strlen($messageData->roomName) > 0) {
    				if(!isset($this->_chatRooms[$messageData->roomName])) {
    					$this->_chatRooms[$messageData->roomName] = array("texto"=>array(),"owner_sala"=>$conectionID,"users"=>array());
    					//Time to notify to all users, to update room list
    					$this->addUserToChatroom($messageData->roomName,$conectionID);
    					$this->refreshChatRoomsList();
    				}
    				else {
    					//A room with that name already exists
    					$this->sendErrorMessage($conectionID,"A roomname with that name, already exists");
    				}
    			} else {
    				//Input of roomname is missing
    				$this->sendErrorMessage($conectionID,"The roomname field is missing");
    			}
    			break;
    		case 'deleteRoom':
    			if(isset($messageData->roomName)) {
    				$chatroomName = $messageData->roomName;
    				//Verification about owner of the chat room
    				if($this->_chatRooms[$chatroomName]['owner'] == $conectionID) {
    					$this->removeUsersFromChatroom($messageData->roomName);
    					unset($this->_chatRooms[$messageData->roomName]);
    					//Notify that the room does't exist anymore
    					$this->refreshChatRoomsList();

    				} else {
    					//Not the owner
    					$this->sendErrorMessage($conectionID,"You're not the owner of the room");
    				}
    			}
    			break;
    		case 'whoOnline':
    			$from->send(json_encode(array(
		    		"message_id"=>7,
		    		"userList"=>$this->_onlineData
    			)));
    			break;
    		case 'leaveRoom':
    			if(isset($messageData->roomName) && strlen($messageData->roomName) > 0) {
    				$roomName = $messageData->roomName;
    				//FIRST WE REMOVE THE CHATROOM FROM USER INFO
    				$this->removeUserFromChatroomArray($roomName,$conectionID);
    				//WE REMOVE THE USER FROM THE CHATROOM DATA
    				$this->removeUserFromChatroom($roomName,$conectionID);
    			}
    			break;
    		case 'joinRoom':
    			if(isset($messageData->roomName) && strlen($messageData->roomName) > 0) {
    				$roomName = $messageData->roomName;
                    if(isset($this->_chatRooms[$roomName])) {
        				if(!$this->joinedRoom($roomName,$conectionID)) { 
        					$this->addUserToChatroom($roomName,$conectionID);
        				} else {
        					//Doesn't exist the room
                            $this->sendErrorMessage($conectionID,"You're already on the roomname");
        				}
                    }
                    else {
                        //Doesn't exist the room
                        $this->sendErrorMessage($conectionID,"The roomname doesn't exist");
                    }
    			} else {
    				//Input of roomname is missing
    				$this->sendErrorMessage($conectionID,"The roomname field is missing");
    			}
    			break;
    		case 'chat':
    			if(isset($messageData->roomName)) {
    				$roomName = $messageData->roomName;
                    if(isset($this->_chatRooms[$roomName])) {
        				if($this->joinedRoom($roomName,$conectionID)) {
        					$arrayText = array(
        						"senderID"=>$conectionID,
    							"message"=>$messageData->text,
    							"profileName"=>$this->_connections[$conectionID]->Name,
    							"timestamp"=>time() 
    						);
    						array_push($this->_chatRooms[$roomName]['texto'], $arrayText);
        					$this->updateChatroom($roomName,$arrayText);
                        }
                        else {
                            //Doesn't exist the room
                            $this->sendErrorMessage($conectionID,"You're not on the roomname");
                        }
                    } else {
                        //Doesn't exist the room
                        $this->sendErrorMessage($conectionID,"The roomname doesn't exist");
                    }
    			}
    			break;
    		case 'profileData':
    			if(isset($messageData->actionProfile)) {
    				switch ($messageData->actionProfile) {
    					case 'modifyProfile':
    						$this->_connections[$conectionID]->Name = $messageData->newName;
    						$this->_connections[$conectionID]->publicDescription = $messageData->newDescription;
    						break;
    					
    					case 'viewProfile':
    						if(isset($messageData->profileID)) {
    							$profID = $messageData->profileID;
    						} else {
    							$profID = $conectionID;
    						}


    						if(isset($this->_connections[$profID])) {
    							$from->send(json_encode(array(
						    		"message_id"=>5,
						    		"profileName"=>$this->_connections[$profID]->Name,
						    		"profileDescription"=>$this->_connections[$profID]->publicDescription
				    			)));
    						}
    						break;
    				}	    			
	    		}
    			break;
            case 'detailsRoom':
                if(isset($messageData->roomName) && strlen($messageData->roomName) > 0) {
                    $roomName = $messageData->roomName;
                    $this->getUserCountRoom($roomName,$from);
                }
                break;
    	}
    }

    private function getUserCountRoom($roomName,$connectedUser) {
        $contador = count($this->_chatRooms[$roomName]['users']);
        if(isset($this->_connections[$connectedUser])) {
            $this->_connections[$connectedUser]->send(json_encode(array(
                "message_id"=>9,
                "total"=>$contador
            )));
        }
    }

    public function onClose(ConnectionInterface $conn) {
    	//WE REMOVE THE USER FROM ALL CHATROOMS array
    	$this->removeUserFromAllChatrooms($conn->resourceId);
    	unset($this->_connections[$conn->resourceId]);
    	unset($this->_onlineData[$conn->resourceId]);
    	echo "[DISCONNECT] ConnectionID ".$conn->resourceId." left the server\n";
    }

    public function onError(ConnectionInterface $conn, \Exception $e) {

    }

    //----- FUNCTIONS-----
    private function joinedRoom($roomName,$conectionID) {
    	if(in_array($roomName, $this->_joinedUsersRoom[$conectionID])) {
    		return true;
    	}
    	return false;
    }

    private function updateChatroom($roomName,$newText) {
    	if(isset($this->_chatRooms[$roomName])) {
    		foreach ($this->_chatRooms[$roomName]['users'] as $connectedUser) {
    			if(isset($this->_connections[$connectedUser])) {
	    			$this->_connections[$connectedUser]->send(json_encode(array(
			    		"message_id"=>3,
			    		"chatroom_name"=>$roomName,
			    		"newText"=>$newText['message'],
			    		"senderName"=>$newText['profileName'],
			    		"senderTime"=>$newText['timestamp']
	    			)));
	    		}
    		}
    	}
    }

    /*
	REMOVE a chatroom from the array of the user
    */
    private function removeUserFromChatroomArray($roomName,$conectionID) {
    	$valorClave = array_search($roomName,$this->_joinedUsersRoom[$conectionID]);
        if($valorClave >= 0) {
            unset($this->_joinedUsersRoom[$conectionID][$valorClave]);
        }
    }

    /*
	Remove a user (conectionID) FROM the array of the chatroom
    */
    private function removeUserFromChatroom($roomName,$conectionID) {
    	$valorClave = array_search($conectionID,$this->_chatRooms[$roomName]['users']);
        if($valorClave >= 0) {
            unset($this->_chatRooms[$roomName]['users'][$valorClave]);
        }
    }

    private function removeUsersFromChatroom($roomName) {
    	foreach ($this->_chatRooms[$roomName]['users'] as $userConectionID) {
    		$this->removeUserFromChatroomArray($roomName,$userConectionID);
    	}
    }

    private function removeUserFromAllChatrooms($conectionID) {
    	foreach ($this->_joinedUsersRoom[$conectionID] as $roomName) {
    		//Remove user from the current chatroom
    		$this->removeUserFromChatroom($roomName,$conectionID);
    	}
    }

    private function addUserToChatroom($roomName,$conectionID) {
    	array_push($this->_chatRooms[$roomName]['users'], $conectionID);
    	array_push($this->_joinedUsersRoom[$conectionID], $roomName);
    	if(isset($this->_connections[$conectionID])) {
	    	$this->_connections[$conectionID]->send(json_encode(array(
	    		"message_id"=>4,
	    		"newChatRoom"=>$roomName,
	    		"previousText"=>$this->_chatRooms[$roomName]['texto']
	    	)));
            $this->getUserCountRoom($roomName,$conectionID);
	    }
    }

    private function sendErrorMessage($conectionID,$message) {
    	//To check and avoid get a crash on server script
    	if(isset($this->_connections[$conectionID])) {
	    	$this->_connections[$conectionID]->send(json_encode(array(
	    		"message_id"=>2,
	    		"message"=>$message
	    	)));
	    }
    }

    private function refreshChatRoomsList() {
    	$roomsNames = array();
    	foreach ($this->_chatRooms as $key => $value) {
    		array_push($roomsNames, $key);
    	}



    	foreach ($this->_connections as $connID) {
    		$connID->send(json_encode(array(
    			"message_id"=>6,
    			"roomList"=>$roomsNames
    		)));
    	}
    }

    private function showCurrentRoomsList($conID) {
    	$roomsNames = array();
    	foreach ($this->_chatRooms as $key => $value) {
    		array_push($roomsNames, $key);
    	}

    	$this->_connections[$conID]->send(json_encode(array(
			"message_id"=>6,
			"roomList"=>$roomsNames
		)));	
    }
}

?>