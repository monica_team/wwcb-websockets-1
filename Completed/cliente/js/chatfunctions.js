$(document).ready(function() {	

	var loggedRooms = [];
	var connectedInfo = 0;
	var conexionWebsocket = 'ws://localhost:9800';

	$.fn.alreadyInRoom = function(roomName) {
		return loggedRooms.indexOf(roomName);
	};

	const socketOpenListener = (event) => {
		$(".alertsConnection").hide();
		$("#conectionOK").show();
		console.log("connected to the server");
		$("#connectionDetails").hide();
		$("#chatContent").show();
		connectedInfo = 1;
		/*--NAVBAR---*/
		$(".itemsOff").hide();
		$(".itemsLogged").show();		
	};

	const socketMessageListener = (e) => {
		var datoMensaje = JSON.parse(e.data);
		switch(parseInt(datoMensaje.message_id)) {
			case 1: /*--- WELCOME -----*/
				if(parseInt(datoMensaje.registered) == 0) {
					$("#profileWelcome").modal('show');
				} else {
					$("#profileWelcome").modal('hide');
				}
				break;
			case 3:
				/*--- MENSAJE NUEVO ------*/
				var htmlChatrooms =  '<div>['+datoMensaje.senderTime+'] From: '+datoMensaje.senderName+'<br/>'+datoMensaje.newText+'</div>';
				$("#chatroom-"+datoMensaje.chatroom_name).append(htmlChatrooms);
				break;
			case 4:
				/*--- MENSAJES ANTERIORES ------*/
				var htmlChatrooms = "";
				if(parseInt(datoMensaje.previousText.length) > 0) {
					$.each(datoMensaje.previousText, function(idx, obj) {
						htmlChatrooms += '<div>['+obj.timestamp+'] From: '+obj.profileName+'<br/>'+obj.message+'</div>';
					});
					$("#chatroom-"+datoMensaje.newChatRoom).html(htmlChatrooms);
				}
				break;
			case 5:
				$("#profileDetails #changeNickname").val(datoMensaje.profileName);
				$("#profileDetails #changeDescription").val(datoMensaje.profileDescription);
				$("#profileDetails").modal('show');
				break;
			case 6: 
				/*---UPDATE CURRENT CHATROOM LIST ---*/
				var htmlChatrooms = '';
				$.each(datoMensaje.roomList, function(idx, obj) {
					htmlChatrooms += '<li class="list-group-item joinChannel">'+obj+'</li>';
				});
				$("#channelList").html(htmlChatrooms);
				break;
			case 8:
				$("#countUsers").html(datoMensaje.totalUsers);
				break;
			case 9:
				$("#userCount").html(datoMensaje.total);
				break;
		}
	};

	const socketCloseListener = (event) => {
		/*--NAVBAR---*/
		$(".itemsOff").show();
		$(".itemsLogged").hide();

		$(".alertsConnection").hide();
		connectedInfo = 0;
		conn = new WebSocket(conexionWebsocket);
		console.log("Connecting to websocket: "+conexionWebsocket);
		conn.addEventListener('open', socketOpenListener);
	   	conn.addEventListener('message', socketMessageListener);
	   	conn.addEventListener('close', socketCloseListener);
	   	conn.addEventListener('error', socketErrorListener);
	};

	const socketErrorListener = (event) => {
		/*--NAVBAR---*/
		$(".itemsOff").show();
		$(".itemsLogged").hide();

		$(".alertsConnection").hide();
		$("#conectionError").show();
		connectedInfo = 0;
	};


	$(document).on('click','#changeConectionDestination', function() {
		conexionWebsocket = 'ws://'+$("#ipDestination").val()+':9800';
		$("#conectionPending").show();
		$("#connectionDetails").hide();
		socketCloseListener();
	});

	$(document).on('click','#createChannel', function()  {
		$("#dialogCreateChannel").modal('show');
	});

	$(document).on('click','.joinChannel', function() {
		var chName = $(this).html();
		$("#channelName").html(chName);

		if($(this).alreadyInRoom(chName) == -1) {
			loggedRooms.push(chName);
			var htmlNewChat = '<div id="chatroom-'+chName+'" class="globalchatrooms"></div>';
			$("#channelText").append(htmlNewChat);
			conn.send(JSON.stringify({tipo: 'joinRoom', roomName: chName }));
		} else {
			conn.send(JSON.stringify({tipo: 'detailsRoom', roomName: chName }));
		}

		$(".globalchatrooms").hide();
		$("#chatContainer").show();
		$("#chatroom-"+chName).show();
		$(".joinChannel").removeClass("active");
		$(this).addClass("active");
	});

	$(document).on('click','#newChannel', function() {
		var channelInput = $("#newChannelName").val();
		conn.send(JSON.stringify({tipo: 'createRoom', roomName: channelInput }));
		$("#dialogCreateChannel").modal('hide');
		$("#newChannelName").val("");
	})

	$(document).on('click','#firstWelcome', function() {
		var nicknameInput = $("#newName").val();
		conn.send(JSON.stringify({tipo: 'welcome_data', nickname: nicknameInput }));
	});

	$(document).on('click','#viewProfile', function() {
		conn.send(JSON.stringify({tipo: 'profileData', actionProfile: 'viewProfile' }));
	});

	$(document).on('click','#modifyProfile', function() {
		var inputName = $("#changeNickname").val();
		var inputDescription = $("#changeDescription").val();
		conn.send(JSON.stringify({tipo: 'profileData', actionProfile: 'modifyProfile', newName:inputName , newDescription:inputDescription }));
		$("#changeNickname").val("");
		$("#changeDescription").val("");
		$("#profileDetails").modal('hide');
	});

	$(document).on('click','#sendTextChat', function() {
		var currentChatRoom = $("#channelList .active").html();
		var inputText = $("#textChat").val();
		conn.send(JSON.stringify({tipo: 'chat', roomName: currentChatRoom, text: inputText }));
		$("#textChat").val("");
	});
});