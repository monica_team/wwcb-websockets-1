$(document).ready(function() {	

	var loggedRooms = [];
	var connectedInfo = 0;
	var conexionWebsocket = 'ws://localhost:9800';

	$.fn.alreadyInRoom = function(roomName) {
		return loggedRooms.indexOf(roomName);
	};

	const socketOpenListener = (event) => {
		$(".alertsConnection").hide();
		$("#conectionOK").show();
		console.log("connected to the server");
		$("#connectionDetails").hide();
		$("#chatContent").show();
		connectedInfo = 1;
		/*--NAVBAR---*/
		$(".itemsOff").hide();
		$(".itemsLogged").show();		
	};

	const socketMessageListener = (e) => {
		var datoMensaje = JSON.parse(e.data);
		switch(parseInt(datoMensaje.message_id)) {
			case 1: /*--- WELCOME -----*/
				if(parseInt(datoMensaje.registered) == 0) {
					$("#profileWelcome").modal('show');
				} else {
					$("#profileWelcome").modal('hide');
				}
				break;
			case 3:
				
				break;
			case 4:

				break;
			case 5:
				$("#profileDetails #changeNickname").val(datoMensaje.profileName);
				$("#profileDetails #changeDescription").val(datoMensaje.profileDescription);
				$("#profileDetails").modal('show');
				break;
			case 6:
				/*---UPDATE CURRENT CHATROOM LIST ---*/
				var htmlChatrooms = '';
				$.each(datoMensaje.roomList, function(idx, obj) {
					htmlChatrooms += '<li class="list-group-item joinChannel">'+obj+'</li>';
				});
				$("#channelList").html(htmlChatrooms);
				break;
			case 8:
				$("#countUsers").html(datoMensaje.totalUsers);
				break;
		}
	};

	const socketCloseListener = (event) => {
		/*--NAVBAR---*/
		$(".itemsOff").show();
		$(".itemsLogged").hide();

		$(".alertsConnection").hide();
		connectedInfo = 0;
		conn = new WebSocket(conexionWebsocket);
		console.log("Connecting to websocket: "+conexionWebsocket);
		conn.addEventListener('open', socketOpenListener);
	   	conn.addEventListener('message', socketMessageListener);
	   	conn.addEventListener('close', socketCloseListener);
	   	conn.addEventListener('error', socketErrorListener);
	};

	const socketErrorListener = (event) => {
		/*--NAVBAR---*/
		$(".itemsOff").show();
		$(".itemsLogged").hide();

		$(".alertsConnection").hide();
		$("#conectionError").show();
		connectedInfo = 0;
	};


	$(document).on('click','#changeConectionDestination', function() {
		conexionWebsocket = 'ws://'+$("#ipDestination").val()+':9800';
		$("#conectionPending").show();
		$("#connectionDetails").hide();
		socketCloseListener();
	});

	$(document).on('click','#createChannel', function()  {
		$("#dialogCreateChannel").modal('show');
	});

	$(document).on('click','.joinChannel', function() {
		
	});

	$(document).on('click','#newChannel', function() {
		var channelInput = $("#newChannelName").val();
		conn.send(JSON.stringify({tipo: 'createRoom', roomName: channelInput }));
		$("#dialogCreateChannel").modal('hide');
		$("#newChannelName").val("");
	})

	$(document).on('click','#firstWelcome', function() {
		var nicknameInput = $("#newName").val();
		conn.send(JSON.stringify({tipo: 'welcome_data', nickname: nicknameInput }));
	});

	$(document).on('click','#viewProfile', function() {
		
	});

	$(document).on('click','#modifyProfile', function() {
		
	});

	$(document).on('click','#sendTextChat', function() {
		
	});
});